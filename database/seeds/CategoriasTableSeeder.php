<?php

use Illuminate\Database\Seeder;

class CategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categorias')->insert
            (
                [
                'descricao' => 'SGBD',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
                ]
            );
        
         DB::table('categorias')->insert
            (
                [
                'descricao' => 'Programação',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
                ]
            );
         
         DB::table('categorias')->insert
            (
                [
                'descricao' => 'Engenharia de Software',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
                ]
            );
        
        
    }
}
