<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

// rota para a página inicial do projeto 
Route::get('/', function () {
    return view('welcome');
});

// rota para a página de teste
Route::get('admin', function () {
    return view('admin_template');
});

//rota para home (criado pelo laravel auth)
Route::get('/home', 'HomeController@index')->name('home');

// rota para  o painel de controle
Route::resource('painel', 'PainelController');

// rota para o admin teste
Route::get('/admin2',['uses'=>'TesteController@index']); 


//Rota para usuários
Route::resource('usuarios', 'UsuarioController');
//Rota para  categorias de notícias
Route::resource('categorias', 'CategoriaController');
//Rota para notícias
Route::resource('noticias', 'NoticiaController');


// Rota para a página sobre
Route::get('/sobre', 'PainelController@sobre')->name('sobre');



