  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Menu Principal</li>
        <!-- Optionally, you can add icons to the links -->
        <li><a href="{{ route('painel.index') }}"><i class="fa fa-home"></i> <span>Início</span></a></li>
        <li><a href="{{ route('usuarios.index') }}"><i class="fa fa-user"></i> <span>Usuários</span></a></li>
        
        <li class="treeview">
          <a href="#"><i class="fa fa-newspaper-o"></i> <span>Notícias</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('noticias.index') }}">Listar</a></li>
            <li><a href="{{ route('categorias.index') }}">Categorias</a></li>
          </ul>
        </li>
        
        <li><a href="/sobre"><i class="fa fa-info-circle"></i> <span>Sobre</span></a></li>



      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>