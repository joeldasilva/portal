@extends('tema.admin_template')

@section('titulo') 
Projeto SI 
@stop

@section('titulo_conteudo')
Notícias
@stop

@section('breadcrumb')
<li class="active">Notícias</li>
@endsection

@section('acoes')
<a href="{{ route('noticias.create') }}" class="btn btn-flat btn-sm btn-primary" data-toggle="tooltip" title="Novo">
    <i class="fa fa-plus"></i> Novo
</a>
<a href="{{ route('painel.index') }}" class="btn btn-sm btn-flat btn-default" data-toggle="tooltip" title="Voltar">
  <i class="fa fa-reply"></i> Voltar
</a>
@endsection


@section('conteudo')

    <div class="row">
       <div class="col-md-12">
       @include('tema.mensagem')
           <div class="box box-solid">   
               <div class="box-body table-responsive">     
                   <table id="tnoticias" class="table table-bordered table-hover dataTable dt-responsive nowrap">
                       <thead>
                           <tr>
                               <th>#</th>
                               <th>Texto</th>
                               <th>Data</th>
                               <th>Categoria</th>
                               <th>Situação</th>

                               <th class="text-right no-print">Ações</th>
                           </tr>
                       </thead>
                       <tbody>
                           @foreach($registro  as $r)
                               <tr>
                                   <td>{{ $r->id }}</td>
                                   <td>{{ $r->texto }}</td>
                                   <td>{{ $r->data }}</td>
                                   <td>{{ $r->categoria->descricao }}</td>
                                   <td>{{ $r->situacao}}</td>

                                   <td class="text-right">

                                   {!! Form::open(['route' => ['noticias.destroy', $r->id], 'method' => 'DELETE', 'onsubmit' => 'return confirm("Excluir o Registro ?")' ]) !!}

                                   <a href="{{ route('noticias.edit', $r->id) }}" class="btn btn-flat btn-sm btn-primary" data-toggle="tooltip" title="Editar">
                                       <i class="fa fa-pencil"></i>
                                   </a>
                                   <button type="submit" class="delete btn btn-sm btn-flat btn-danger" data-toggle="tooltip" title="Excluir">
                                       <i class="fa fa-trash"></i>
                                   </button>  

                                   {!! Form::close() !!}
                                   </td> 


                               </tr>
                           @endforeach
                       </tbody>
                   </table>
               </div>
           </div>
       </div>
   </div>

@endsection

