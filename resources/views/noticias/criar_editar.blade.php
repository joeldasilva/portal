@extends('tema.admin_template')

@section('titulo', 'Portal SI')

@section('titulo_conteudo', 'Notícias')

@section('breadcrumb')
<li class="active">Cadastro de Notícias</li>
@endsection

@section('acoes')
<a href="{{ route('noticias.index') }}" class="btn btn-sm btn-flat btn-default" data-toggle="tooltip" title="Voltar">
    <i class="fa fa-reply"></i> Voltar
</a>
@endsection


@section('conteudo')
@include('tema.mensagem')

@if(isset($noticia))
{!! Form::model($noticia, ['method' => 'PATCH', 'route' => ['noticias.update', $noticia->id]]) !!}
@else
{!! Form::model(new App\Noticia, ['route' => ['noticias.store']]) !!}

@endif

<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-body">  
                <div class="row">
                    <div class="col-md-12">                     
                        
                        <div class="form-group">
                            {!! Form::label('situacao', 'Situação', array('class' => 'control-label')) !!}  
                            {!! Form::select('situacao', ['Ativa' => 'Ativa', 'Inativa' => 'Inativa'] ,null, array('class' => 'form-control', 'required')) !!}
                        </div> 


                        <div class="form-group {{ ($errors->has('texto') ? 'has-error' : null ) }}">
                            {!! Form::label('texto', 'Postagem', array('class' => 'control-label')) !!}   
                            {!! $errors->first('texto', '<span class="text-danger"> ( :message )</span>') !!} 
                            {!! Form::textarea('texto', null, array('class' => 'form-control ', 'placeholder' => 'Postagem', 'style' => 'width:100%;', 'required')) !!}
                        </div>  

                        <div class="form-group ">
                            {!! Form::label('id_categoria', 'Categoria da Notícia', array('class' => 'control-label')) !!}   
                            {!! Form::select('id_categoria', $categoria, null, ['class' => 'form-control select']) !!}
                        </div>  
                    </div>        
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" data-toggle="tooltip" title="Salvar" class="btn btn-flat btn-sm btn-success pull-right">
                    <i class="fa fa-save"></i> Salvar
                </button>
            </div>            
        </div>
    </div>
</div>

{!! Form::close() !!}

@endsection






    
 

