@extends('tema.admin_template')

@section('titulo', 'Usuários')

@section('titulo_conteudo', 'Cadastrar Usuário')

@section('breadcrumb')
<li class="active">Cadastrar Usuário</li>
@endsection

@section('acoes')
<a href="{{ route('usuarios.index') }}" class="btn btn-sm btn-flat btn-default" data-toggle="tooltip" title="Voltar">
    <i class="fa fa-reply"></i> Voltar
</a>
@endsection

@section('conteudo')
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-body">
              @include('tema.mensagem')

                {!! Form::open(['method' => 'POST', 'route' => ['usuarios.store']]) !!}

                    @include('usuarios.formulario')

                {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection