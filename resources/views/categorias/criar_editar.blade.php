@extends('tema.admin_template')

@section('titulo', 'Portal SI')

@section('titulo_conteudo', 'Categorias')

@section('breadcrumb')
<li class="active">Cadastro de Categorias</li>
@endsection

@section('acoes')
<a href="{{ route('categorias.index') }}" class="btn btn-sm btn-flat btn-default" data-toggle="tooltip" title="Voltar">
    <i class="fa fa-reply"></i> Voltar
</a>
@endsection


@section('conteudo')
@include('tema.mensagem')

@if(isset($registro))
{!! Form::model($registro, ['method' => 'PATCH', 'route' => ['categorias.update', $registro->id]]) !!}
@else
{!! Form::model(new App\Categoria, ['route' => ['categorias.store']]) !!}

@endif

<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-body">  
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group {{ ($errors->has('descricao') ? 'has-error' : null ) }}">
                            {!! Form::label('descricao', 'Descrição', array('class' => 'control-label')) !!}  
                            {!! $errors->first('descricao', '<span class="text-danger"> ( :message )</span>') !!}    
                            {{ Form::text('descricao', old('descricao'), array('class' => 'form-control', 'placeholder' => 'Categoria', 'autofocus' => 'true', 'required')) }} 
                        </div>                        
                    </div>        
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" data-toggle="tooltip" title="Salvar" class="btn btn-flat btn-sm btn-success pull-right">
                    <i class="fa fa-save"></i> Salvar
                </button>
            </div>            
        </div>
    </div>
</div>

{!! Form::close() !!}

@endsection






    
 

