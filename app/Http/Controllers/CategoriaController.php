<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Categoria;
use App\Http\Requests\RequestCategoria;

class CategoriaController extends Controller
{
    public function __construct()
	{
	    $this->middleware('auth');
	}


 
    public function index() {
    	$registro = Categoria::all();
     	return view('categorias.listar',compact('registro'));
     }
     

    public function create()
    {
    	return view("categorias.criar_editar");
      
    }

	public function store(RequestCategoria $request){
       try {
            DB::beginTransaction();

            Categoria::create($request->all());

        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollBack();
            return back()->with('error','Não foi possível cadastrar!');

        }            

            DB::commit();
            return redirect()->route('categorias.index')->with('notice', 'Registro  Cadastrado com Sucessso!'); 
    }


	public function edit($id){
		$registro  = Categoria::find($id);
     	return view('categorias.criar_editar',compact('registro')); 

	}


	 public function update(RequestCategoria $request, $id){
	
		$categoria = Categoria::find($id);
		$categoria->descricao=$request->get('descricao');


	 	try {
	 		 $categoria->save();

        } catch (\Illuminate\Database\QueryException $e) {
            return back()->with('error','Não foi possível alterar o registro!');
        }
        
        return redirect()->route('categorias.index')->with('notice', 'Registro Atualizado com Sucesso!');

    }



  	public function destroy($id){

        try {
            Categoria::find($id)->delete();
        } catch (\Illuminate\Database\QueryException $e) {
            return redirect()->route('categorias.index')->with('error','Não foi possível excluir o registro!');
        }
        
        return redirect()->route('categorias.index')->with('notice', 'Registro Excluído com Sucesso!'); 
    } 




}
