<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class TesteController extends Controller {

    public function index() {
        $dados['tarefas'] = [
                [
                        'nome' => 'Seminario de Andamento',
                        'progresso' => '45',
                        'cor' => 'danger'
                ],
                [
                        'nome' => 'Artigo para o EATI 2018',
                        'progresso' => '50',
                        'cor' => 'warning'
                ],
                 [
                        'nome' => 'Artigo do TCC',
                        'progresso' => '30',
                        'cor' => 'danger'
                ],

                 [
                        'nome' => 'Implementações do TCC',
                        'progresso' => '10',
                        'cor' => 'info'
                ]
        ];
        return view('teste')->with($dados);
    }

}
