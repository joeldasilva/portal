<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class PainelController extends Controller
{
    
     public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $dados = array(
                    'qtdusuarios'  => count(User::all()) 
                    ); 

        return view('painel',compact('dados'));

    }
    
    public function sobre()
    {
        return view('sobre');
    }
}
