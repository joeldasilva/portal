<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class RequestTiposObjetos extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
       return Auth::check();

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
           switch($this->method())
        {
            case 'POST':
            {
                return [
                    'descricao' => 'required|min:3|max:255|unique:tipo_objetos',
                ];
            }
            case 'PATCH':
            {
                return [
                     'descricao' => 'required|min:3|max:255',
                ];
            }
            default:break;
        }
    }
}



